#include <cmath>
#include <iostream>

using namespace std;

int Suma(int n, const int *arr) {
    int s = 0;
    for (int i = 0; i < n; i++) {
        s += arr[i];
    }
    return s;
}

short PierwiastkiKwadratowe(double a, double b, double c, double &x1, double &x2) {
    double d = b * b - 4 * a * c;
    if (d == 0) {
        x1 = -b / (2 * a);
        return 1;
    } else if (d > 0) {
        x1 = (-b - sqrt(d)) / (2 * a);
        x2 = (-b + sqrt(d)) / (2 * a);
        if (x1 > x2) {
            double tmp = x1;
            x1 = x2;
            x2 = tmp;
        }
        return 2;
    } else {
        return 0;
    }
}

void Fibonacci(int *arr, int id) {
    if (id < 0) {
        return;
    }
    arr[0] = 0;
    if (id == 0)
        return;
    arr[1] = 1;
    if (id == 1)
        return;
    for (int i = 2; i <= id; i++) {
        arr[i] = arr[i - 1] + arr[i - 2];
    }
}

bool SredniaWazona(const double *arr, int n, double &result) {
    double sum = 0;
    for (int i = 1; i < 2 * n - 1; i += 2) {
        if (arr[i] < 0 || arr[i] > 1)
            return false;
        else
            sum += arr[i];
    }
    if (sum > 1)
        return false;
    double used = 0;
    result = 0;
    for (int i = 0; i < 2 * n - 2; i += 2) {
        result += arr[i] * arr[i + 1];
        used += arr[i + 1];
    }
    result += arr[2 * n - 2] * (1 - used);
    return true;
}

int Licznik(bool flag, int &curr) {
    static int currTrue = 0, currFalse = 0;
    if (flag) {
        curr = ++currTrue;
    } else {
        ++currFalse;
    }
    return currFalse;
}

int main() {
    int arr[6] = {1, -2, 3, -4, 5, -6};
    int n = 6;
    cout << Suma(n, arr) << endl;
    arr[1] = arr[1] * -1;
    cout << Suma(n, arr) << endl;

    double c = 1;
    double wagi[] = {0.1, 0.3, 0.15, 0.05, 0.2};
    double srednia[2 * n - 1];
    for (int i = 0; i < n; i++) {
        srednia[2 * i] = arr[i];
        srednia[2 * i + 1] = wagi[i];
    }
    srednia[2 * n - 2] = arr[n - 1];
    cout << SredniaWazona(srednia, n, c) << " ";
    cout << c << endl;
    srednia[2 * n - 3] = 0.45;
    cout << SredniaWazona(srednia, n, c) << " ";
    cout << c << endl;
    double x1 = 1, x2 = 1;
    cout << PierwiastkiKwadratowe(1, 2, 1, x1, x2);
    cout << " " << x1 << " " << x2 << endl;
    int tab[10] = {0};
    Fibonacci(tab, 15);
    for (int i = 0; i < 15; i++) {
        cout << tab[i] << " ";
    }
    cout << endl;

    cout << Licznik(true, n) << " ";
    cout << n << endl;
    cout << Licznik(true, n) << " ";
    cout << n << endl;
    cout << Licznik(false, n) << " ";
    cout << n << endl;
    return 0;
}
